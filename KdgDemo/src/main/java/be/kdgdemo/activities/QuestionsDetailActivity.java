package be.kdgdemo.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.reflect.TypeToken;

import be.kdgdemo.R;
import be.kdgdemo.adapter.AnswerAdapter;
import be.kdgdemo.json.GsonHelper;
import be.kdgdemo.model.Answer;
import be.kdgdemo.model.Question;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.view.inputmethod.EditorInfo.IME_ACTION_SEND;
import static android.widget.TextView.OnEditorActionListener;

/**
 * @author Jo Somers
 */
public class QuestionsDetailActivity extends BaseActivity implements Callback<Question> {

    private static final String TAG = QuestionsDetailActivity.class.getSimpleName();

    @InjectView(R.id.question)
    TextView questionTextView;

    @InjectView(R.id.listview)
    ListView listView;

    @InjectView(R.id.answer)
    EditText answerInputEditText;

    @InjectView(R.id.submitAnswer)
    ImageView submitAnswerView;

    private Question question;

    private AnswerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_questions_detail);

        ButterKnife.inject(this);

        if (getActionBar() != null) {
            getActionBar().setHomeButtonEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initQuestion();
        initEditorActionListener();

        questionTextView.setText(question.getQuestion());

        adapter = new AnswerAdapter(this, question.getAnswers());
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshQuestion();
    }

    @Override
    public void success(
            final Question newQuestion,
            final Response response) {

        question = newQuestion;
        questionTextView.setText(question.getQuestion());
        adapter.setAnswers(question.getAnswers());
    }

    @Override
    public void failure(final RetrofitError error) {
        handleFailure(error);
    }

    @OnClick(R.id.submitAnswer)
    public void onSubmitAnswerClicked() {
        submitAnswer();
    }

    private void initQuestion() {
        final GsonHelper<Question> gsonParser = new GsonHelper<Question>(new TypeToken<Question>() {
        }.getType());

        question = gsonParser.fromJson(getIntent().getStringExtra("question"));
    }

    private void initEditorActionListener() {
        answerInputEditText.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == IME_ACTION_SEND) {
                    submitAnswer();
                    handled = true;
                }

                return handled;
            }
        });
    }

    private void refreshQuestion() {
        getService().getQuestion(question.getId(), this);
    }

    private void handleFailure(final Throwable throwable) {
        getVisualisation().showError(getString(R.string.tFailure));

        Log.e(TAG, throwable.toString());
    }

    private void submitAnswer() {
        answerInputEditText.setEnabled(false);
        submitAnswerView.setEnabled(false);

        final Callback<Answer> callback = new Callback<Answer>() {
            @Override
            public void success(
                    final Answer answer,
                    final Response response) {

                answerInputEditText.setText("");
                answerInputEditText.setEnabled(true);
                submitAnswerView.setEnabled(true);
                refreshQuestion();
            }

            @Override
            public void failure(final RetrofitError error) {
                answerInputEditText.setEnabled(true);
                submitAnswerView.setEnabled(true);
                handleFailure(error);
            }
        };

        final String answerText =
                answerInputEditText != null
                        && answerInputEditText.getText() != null ? answerInputEditText.getText().toString() : "";

        final Answer answer = new Answer();
        answer.setQuestionId(question.getId());
        answer.setAnswer(answerText);

        getService().submitAnswer(answer, callback);
    }

}
