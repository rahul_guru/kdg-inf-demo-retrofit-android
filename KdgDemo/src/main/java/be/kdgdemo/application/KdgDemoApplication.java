package be.kdgdemo.application;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import be.kdgdemo.R;
import be.kdgdemo.service.KdgService;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

import static be.kdgdemo.json.Iso8601DateTimeTypeAdapter.ISO_8601_DATE_TIME_TYPE_ADAPTER;
import static be.kdgdemo.json.Iso8601LocalDateTypeAdapter.ISO_8601_LOCAL_DATE_TYPE_ADAPTER;

/**
 * @author Jo Somers
 */
public class KdgDemoApplication extends Application {

    private static KdgService kdgService;

    @Override
    public void onCreate() {
        super.onCreate();

        kdgService = createKdgDemoService(this);
    }

    public static KdgService getKdgService() {
        return kdgService;
    }

    private KdgService createKdgDemoService(Context context) {
        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(DateTime.class, ISO_8601_DATE_TIME_TYPE_ADAPTER)
                .registerTypeAdapter(LocalDate.class, ISO_8601_LOCAL_DATE_TYPE_ADAPTER)
                .create();

        return new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setEndpoint(context.getString(R.string.baseUrl))
                .build()
                .create(KdgService.class);
    }

}
