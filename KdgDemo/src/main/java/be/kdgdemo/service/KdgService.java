package be.kdgdemo.service;

import java.util.List;

import be.kdgdemo.model.Answer;
import be.kdgdemo.model.Question;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * @author Jo Somers
 */
public interface KdgService {

    @GET("/questions")
    void getQuestions(Callback<List<Question>> callback);

    @POST("/questions")
    void submitQuestion(@Body Question question, Callback<Question> created);

    @DELETE("/questions/{id}")
    void deleteQuestion(@Path("id") String questionId, Callback<Question> callback);

    @GET("/questions/{questionId}")
    void getQuestion(@Path("questionId") String questionId, Callback<Question> callback);

    @POST("/answers")
    void submitAnswer(@Body Answer answer, Callback<Answer> created);

}
